# KUBO!

This project contains all the configs and documentation related to the Kubernetes deployment and using the microservices application _Kubo_.

The application is consisted of three different microservices: customers, bikes and rental.
The rental service is responsible for associating bikes to customers. Once an _agreement_ object is created by the rental service, the bike's availability will change to 'false'. Agreements contain both bike and customer information, therefore, it's necessary to create those in prior to create an agreement (please see the API specifications in the collection.json file).
A Mongo database is necessary by all three services to persist their data.

## Project structure & development

The code base of each service is stored separately in different repositories (see [here](https://gitlab.com/azizoo/kubo/)). After each tagged commit to the repository, a Gitlab CI pipeline will start that performs the following two tasks:
- build a new Docker image (tagged using the git tag)
- push the created image to Gitlab private registry

## How to deploy on Kubernetes

To deploy the application on Kubernetes, you first need the following:
- A Kubernetes cluster (our setup.sh file uses Google Cloud to initiate a cluster on it)
- A MongoDB server (that is being used by all three services)

There's one deployment and associated service for each microservice and one single ingress controller on top of the services. The ingress resource uses an static IP address (previously created on Google Cloud platform) and uses an external Google Cloud managed Load Balancer to act an application gateway and do the routing (we're also configuring SSL there).

Once your cluster is ready, you can apply manifest files (yaml files) in `manifests` directory and create your deployment, services and ingress resources. However, please make sure your environment has values for the following variables:

NOTE: All the steps above are done in the `setup.sh` file. Please read the comments there. Please don't forget to create your secrets for the database and image pulling.

## Using the application

Once deployed you can check the status of your pods using the command `kubectl get pods`. If all running and healthy, you can test the application by sending simple HTTP requests to it. In `docs` directory, you'll find a Postman collection file that easily can be imported and used on Postman. Before sending any requests, please make sure your collection's environment variable for `URL` is set correctly. It needs to have the Ingress controller's external IP address (your load balancer) as value. 