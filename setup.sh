#!/bin/bash

# enable Kubernetes on GCP
gcloud services enable container.googleapis.com

# create a Kubernetes cluster with 3 nodes using the zone in NL
gcloud container clusters create kubo --num-nodes=3 --zone=europe-west4-a

# create a static ip address to be used by Ingress (see the Ingress manifest)
gcloud compute addresses create web-static-ip --global

# add Docker registry secret (change the server value and the env keys if not using GitLab to store images)
kubectl create secret docker-registry registrykey --docker-server=registry.gitlab.com --docker-username=$GITLAB_USER --docker-password=$GITLAB_PSWD --docker-email=$EMAIL

# the following file is in .gitignore. Create your own, if injecting any secrets manually (like the the db secrets used in all example deployment). 
kubectl apply -f ./manifests/secret.yaml

# the follwoing secret is neccesary to store SSL certifactes in a secret to be used by Ingress (see the Ingress manifest)
kubectl create secret tls tls-secret --cert $KUBOTLS_CERT_PATH --key $KUBOTLS_KEY_PATH

# apply deployments and services
kubectl apply -f ./manifests/bikes.yaml 
kubectl apply -f ./manifests/customers.yaml 
kubectl apply -f ./manifests/rental.yaml 
kubectl apply -f ./manifests/home.yaml 

# apply ingress
kubectl apply -f ./manifests/gateway.yaml